﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPad : MonoBehaviour
{
    [Tooltip("Speed Boost Modifier on X axis")]
    public float SpeedBoostX = 20f;
    [Tooltip("Speed Boost Modifier on Z axis")]
    public float SpeedBoostZ = 20f;
    Rigidbody CollidedRB;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("SpeedPad");
        CollidedRB = other.gameObject.GetComponent<Rigidbody>();
        CollidedRB.AddForce(new Vector3(SpeedBoostX, 0f, SpeedBoostZ), ForceMode.Impulse);
    }
}
