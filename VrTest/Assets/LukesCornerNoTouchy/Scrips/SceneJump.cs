﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneJump : MonoBehaviour
{
    bool IsPaused = false;
    public GameObject PauseMenu;
    public GameObject Pointer;
    
    
    private void Update()
    {
        if (OVRInput.Get(OVRInput.Button.Two) && IsPaused == false)
        {
            PauseGame();
            Debug.Log("Pause");
        }
        else if (OVRInput.Get(OVRInput.Button.Two) && IsPaused == true)
        {
            Unpause();
            Debug.Log("UnPause");
        }
    }
    void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);
        Pointer.SetActive(true);
        IsPaused = true;
    }
    void Unpause()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        Pointer.SetActive(false);
        IsPaused = false;
    }
    public void SceneSwitch(int SceneName)
    {
        SceneManager.LoadScene(SceneName);
        Debug.Log("SceneJump");
    }
    public void Quit()
    {
        Application.Quit();
    }

    public void Resume()
    {
        Unpause();
    }
    public void Restart()
    {
        Unpause();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void BackToMenu()
    {
        Unpause();
        SceneSwitch(0);
    }
}
