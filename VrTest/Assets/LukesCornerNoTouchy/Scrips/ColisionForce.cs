﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisionForce : MonoBehaviour
{

    public float CollisionForce = 6;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("PushBack");
            Vector3 ObjectPos;
            Vector3 PlayerPos;
            Vector3 ColisionDirection;
            ObjectPos = gameObject.transform.position;
            PlayerPos = other.transform.position;
            ColisionDirection = PlayerPos - ObjectPos;
            other.attachedRigidbody.AddForce(other.attachedRigidbody.velocity * -1, ForceMode.Impulse);
            other.attachedRigidbody.AddForce(ColisionDirection * CollisionForce, ForceMode.Impulse);
            
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        Vector3 ObjectPos;
        Vector3 PlayerPos;
        Vector3 ColisionDirection;
        ObjectPos = gameObject.transform.position;
        PlayerPos = collision.transform.position;
        ColisionDirection = PlayerPos - ObjectPos;

        collision.rigidbody.AddForce(collision.gameObject.GetComponent<Rigidbody>().velocity * -1, ForceMode.Impulse);
        collision.rigidbody.AddForce(ColisionDirection * CollisionForce, ForceMode.Impulse);
    }

}
