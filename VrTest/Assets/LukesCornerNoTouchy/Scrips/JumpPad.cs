﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
    [Tooltip("How high the player is launched")]
    public float LaunchHeight = 10f;
    Rigidbody CollidedRB;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Jump Pad");
        CollidedRB = other.gameObject.GetComponent<Rigidbody>();
        CollidedRB.AddForce(new Vector3(0f, LaunchHeight, 0f), ForceMode.Impulse);
    }

}
