﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingingObject : MonoBehaviour
{
    public float SwingForce = 10;
    Vector3 ObjPos;
    Vector3 ColiPos;
    Vector3 SwingDirection;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Envioment")
        {
            ObjPos = transform.position;
            ColiPos = other.transform.position;
            SwingDirection = ColiPos - ObjPos;
            
            other.attachedRigidbody.AddForce(-SwingDirection * SwingForce, ForceMode.Force);
        }
    }
}
