﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovenment : MonoBehaviour
{
    public Animator animator;
    [Tooltip("Players Base Walk Speed (WASD)")]
    public float PlayerSpeed = 8f;
    [Tooltip("Players Max Speed")]
    public float maxSpeed = 50f;
    [Tooltip("Players Max Jump Height")]
    public float JumpHeight = 4f;

    Rigidbody Rigi;

    private float xInput;
    private float zInput;


    [Header("Programmer Shit - Dont touch :)")]
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;
    void Awake()
    {
        Rigi = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }
    }
    private void FixedUpdate()
    {
        ProcessInput();
        Animate();
        Move();
        if (Rigi.velocity.magnitude > maxSpeed)
        {
            Rigi.velocity = Rigi.velocity.normalized * maxSpeed;
        }
    }
    private void ProcessInput()
    {

        xInput = Input.GetAxis("Horizontal");
        zInput = Input.GetAxis("Vertical");

    }
    private void Move()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        Rigi.AddForce(new Vector3(xInput, 0f, zInput) * PlayerSpeed);

    }
    private void Jump()
    {
        Rigi.AddForce(new Vector3(0f, JumpHeight, 0f), ForceMode.Impulse);
    }

    private void Animate()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            animator.SetFloat("ControllerY", 1);
            
        }
        else
        {
            animator.SetFloat("ControllerY", -1);
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            
            animator.SetFloat("ControllerZ", -1);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            
            animator.SetFloat("ControllerZ", 1);
        }

        print("Z " + animator.GetFloat("ControllerZ"));
        print(animator.GetFloat("ControllerZ"));
        print("Y " + animator.GetFloat("ControllerY"));
        print(animator.GetFloat("ControllerY"));
    }
}
