﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : MonoBehaviour
{

    public float speed = 100;
    public float timeInteval = 2;
    [Header("Dont Touch "), Tooltip("Dont play with unless you know how it works")]
    public float minHeight = 0f;
    public float maxHeight = 20f;
    float CountDown;
    public bool open;
    public bool closed;
    bool opening;
    bool closing;


    private void Start()
    {
        closing = false;
        opening = false;


    }
    void Update()
    {
        if (opening == false && closing == false)
        {
            CountDown += Time.deltaTime;
            Debug.Log(CountDown);
        }
        
        if (closed == true && CountDown >= timeInteval)
        {
            open = true;
            closed = false;
            opening = true;
            
            CountDown = 0;
        }
        if (open == true && CountDown >= timeInteval )
        {
            closed = true;
            open = false;
            closing = true;
            CountDown = 0;
        }
        if (transform.position.y <= minHeight + 0.1f)
        {
            opening = false;
            
        }
        if (transform.position.y >= maxHeight - 0.1f)
        {
            closing = false;
            
        }

        if (closed == true)
        {
            if (transform.position.y < maxHeight)
            {
                Debug.Log("Closing");
                Debug.Log(transform.position.y);
                transform.Translate(0f, speed * Time.deltaTime, 0f);
            }
        }
        if (open == true)
        {
            if (transform.position.y > minHeight)
            {
                Debug.Log("Opening");
                Debug.Log(transform.position.y);
                transform.Translate(0f, -speed * Time.deltaTime, 0f);
            }
        }



    }

}
