﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoScroll : MonoBehaviour
{

    Scrollbar VertScroll;
    [Tooltip("this Value is a Multiplier")]
    public float ScrollSpeed = 1;
    float ScrollValue;
    void Start()
    {
        VertScroll = gameObject.GetComponent<Scrollbar>();
        
    }

    // Update is called once per frame
    void Update()
    {
        VertScroll.value = ScrollValue;
        ScrollValue += 0.001f * ScrollSpeed;
        if (ScrollValue > 1)
        {
            ScrollValue = 0;
        }
    }
}
