﻿// Author : Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BindPosition : MonoBehaviour
{

    [Tooltip("Target Object")]
    public GameObject BindedObj;
    public float xOffSet;
    public float yOffSet;
    public float zOffSet;



    void Update()
    {
       gameObject.transform.position = BindedObj.transform.position + new Vector3(xOffSet, yOffSet, zOffSet);
    }
}
