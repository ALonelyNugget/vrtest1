﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Debugging : MonoBehaviour
{
    public TextMeshProUGUI[] text;
    //OVRPose tracker;
    public Transform cam;
    public Transform controller;
    public bool DebuggingCam;
    // Update is called once per frame
    private void Start()
    {
       // tracker = OVRManager.tracker.GetPose();
    }
    void Update()
    {
        // tracker = OVRManager.tracker.GetPose();
        //rot = tracker.orientation;

        if (DebuggingCam)
        {
            text[0].text = "x: " + cam.transform.eulerAngles.x;
            text[1].text = "y: " + cam.transform.eulerAngles.y;
            text[2].text = "z: " + cam.transform.eulerAngles.z;
        }
        else
        {

            text[0].text = "x: " + controller.forward.x;
            text[1].text = "y: " + controller.forward.y;
            text[2].text = "z: " + controller.forward.z;

        }                                                                                 
    }
}
