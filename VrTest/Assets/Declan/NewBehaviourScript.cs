﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public Transform pointer;
    public LineRenderer lineRenderer;

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(pointer.position, pointer.forward); 
        RaycastHit hit;

        lineRenderer.SetPosition(0, ray.origin); 
        lineRenderer.SetPosition(1, ray.origin + 100 * ray.direction);
    }
}
