﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovenment1 : MonoBehaviour
{

    public bool VR;

    public Transform controller;
    public Animator animatorHands;
    public Animator animatorBall;

    [Tooltip("Players Base Walk Speed (WASD)")]
    public float PlayerSpeed = 8f;
    [Tooltip("Players Max Speed")]
    public float maxSpeed = 50f;
    [Tooltip("Players Max Jump Height")]
    public float JumpHeight = 4f;
    [Header("Currently Disabled")]
    public float extraJumps = 1;
    int jumpCount = 0;
    Rigidbody Rigi;



    private float xInput;
    private float zInput;
    private Vector3 controllerPos;

    [Header("Programmer Shit - Dont touch :)")]
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;
    void Awake()
    {
        Rigi = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {




    }

    private void FixedUpdate()
    {
        ProcessInput();
        Animate();
        Move();
        if (Rigi.velocity.magnitude > maxSpeed)
        {
            Rigi.velocity = Rigi.velocity.normalized * maxSpeed;
        }

        if (OVRInput.Get(OVRInput.Button.One) && isGrounded)
        {
            Stop();
        }
    }
    private void ProcessInput()
    {

        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            if (controller.forward.z >= 0)
            {

                if (controller.forward.y - 0.5f < 0.1 && controller.forward.y - 0.5f > 0)
                {
                    controllerPos = Vector3.zero;
                }
                else if (controller.forward.y < 0.5)
                {
                    controllerPos.y = (0.5f - controller.forward.y) *2;
                }
                else if (controller.forward.y > 0.5)
                {
                    controllerPos.y = ((controller.forward.y - 0.5f) * -1) *2;
                }
            }
            else
            {
                if (controller.forward.y - 0.5f < 0.1 && controller.forward.y - 0.5f > 0)
                {
                    controllerPos = Vector3.zero;
                }
                else if (controller.forward.y < 0.5)
                {
                    controllerPos.y = (controller.forward.y - 0.5f) * 2;
                }
                else if (controller.forward.y > 0.5)
                {
                    controllerPos.y = ((controller.forward.y - 0.5f) ) * 2;
                }

            }
            controllerPos.x = controller.forward.x;
            controllerPos.z = controller.forward.z;
        }



    }


    private void Move()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        Rigi.AddForce(new Vector3(controllerPos.x, 0, controllerPos.y) * PlayerSpeed);

        controllerPos = Vector3.zero;
    }

    private void Jump()
    {
        Rigi.AddForce(new Vector3(0f, JumpHeight, 0f), ForceMode.Impulse);
    }

    private void Stop()
    {
        Rigi.velocity = Vector3.Lerp(Rigi.velocity,new Vector3(0,0,0),0.05f);
    }

    private void Animate()
    {



        animatorHands.SetFloat("ControllerY", controller.forward.y);
        animatorBall.SetFloat("ControllerY", controller.forward.y);
        animatorHands.SetFloat("ControllerZ", controller.forward.x);
        animatorBall.SetFloat("ControllerZ", controller.forward.x);
    }

}
